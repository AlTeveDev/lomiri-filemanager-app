# File Manager+

File Manager+ is fork of Lomiri File Manager of which the purpose is to be
as powerful as Android file managers are. Currently it only has integrated
code editor, but some more features are planned.

## i18n: Translating filemanagerplus into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/ubports/filemanagerplus

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.

## Useful Links

Here are some useful links with regards to the File Manager+ App development.

* [UBports](https://ubports.com/)
* [Clickable](https://clickable-ut.dev/en/latest/)
* [OpenStore](https://open-store.io/app/filemanager.ubports)
