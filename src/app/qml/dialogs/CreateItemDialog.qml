import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

// TODO: Path validation in inputText: disable buttons if new object name is not valid
// TODO: Handle failure of mkpath/touch

Dialog {
    id: dialog

    property var folderPage
    property var folderModel

    title: i18n.tr("Create Item")
    text: i18n.tr("Enter name for new item")

    TextField {
        id: inputText
        placeholderText: i18n.tr("Item name")
        focus: true
    }

    Button {
        id: createFile
        text: i18n.tr("Create file")
        color: theme.palette.normal.positive
        enabled: inputText.text.trim() != ""

        onClicked: {
            inputText.focus = false
            console.log("Create file accepted", inputText.text)

            var fileName = inputText.text.trim()
            if (folderModel.existsDir(fileName) || folderModel.existsFile(fileName)) {
                PopupUtils.open(Qt.resolvedUrl("FileExists.qml"))
            } else if (folderModel.touch(fileName)) {
                folderPage.tooltipMsg = i18n.tr("Created file '%1'").arg(inputText.text)
                PopupUtils.close(dialog)
            }

        }
    }

    Button {
        id: createFolder
        text: i18n.tr("Create Folder")
        enabled: inputText.text.trim() != ""

        onClicked: {
            inputText.focus = false
            console.log("Create folder accepted", inputText.text)

            var folderName = inputText.text.trim()
            if (folderModel.existsDir(folderName) || folderModel.existsFile(folderName)) {
                PopupUtils.open(Qt.resolvedUrl("FileExists.qml"))
            } else if (folderModel.mkdir(folderName)) {
                folderPage.tooltipMsg = i18n.tr("Created folder '%1'").arg(inputText.text)
                PopupUtils.close(dialog)
            }
        }
    }

    Button {
        id: cancelButton
        text: i18n.tr("Cancel")
        onClicked: {
            PopupUtils.close(dialog)
        }
    }
}
