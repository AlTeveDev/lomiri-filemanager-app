import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: root

    title: i18n.tr("Error")
    text: i18n.tr("Item with this name already exists")

    Button {
        id: okButton
        objectName: "okButton"
        text: i18n.tr("OK")

        onClicked: {
            PopupUtils.close(root)
        }
    }
}
