import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: root

    title: i18n.tr("Save file")
    text: i18n.tr("Enter name for new file")

    property alias fieldText: textField.text
    property alias placeholderText: textField.placeholderText

    property var folderModel

    signal ok(var text)

    TextField {
        id: textField

        Component.onCompleted: {
            textField.select(0, text.length)
        }

        placeholderText: i18n.tr("File name")
        focus: true
    }

    Button {
        id: okButton
        text: i18n.tr("OK")
        enabled: textField.text.trim() != ""
        color: theme.palette.normal.positive

        onClicked: {
            if (folderModel.existsDir(fieldText) || folderModel.existsFile(fieldText)) {
                PopupUtils.open(Qt.resolvedUrl("FileExists.qml"));
            } else {
                console.log(folderView);
                if (editorView) editorView.saveFileAs(folderModel.path + "/" + fieldText);
                folderView.path = folderModel.path;
                PopupUtils.close(root);
                pageStack.pop();
                saveAsMode = false;
            }
            ok(textField.text)
        }
    }

    Button {
        id: cancelButton
        text: i18n.tr("Cancel")

        onClicked: PopupUtils.close(root)
    }
}
