import QtQuick 2.6
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.FileManager.Editor 0.1

Page {
    id: root

    property var model

    property bool searchBarVisible: false

    property bool isWritable: model.isWritable

    function saveFileAs(filePath) {
        console.log("Saving file as", filePath)
        editor.filePath = filePath;
        editor.saveFile();
        isWritable = true;
        // editor.saveFileAs(filePath);
    }

    Component.onCompleted: editorView = root
    Component.onDestruction: editorView = undefined

    readonly property string fileName: editor.fileName

    header: PageHeader {
        id: height
        title: editor.fileName
        // flickable: codeArea.flickable
        // height: extension.height

        // We dont need header separator because we have TextArea frame
        // StyleHints {
        //     dividerColor: "transparent"
        // }

        trailingActionBar.actions: [
            Action {
                iconName: "save"
                text: i18n.tr("Save")
                visible: isWritable
                enabled: editor.modified
                onTriggered: {
                    editor.saveFile()
                    // editor.writeFile(model.filePath, codeArea.text);
                }
            },
            Action {
                iconName: "save-as"
                text: i18n.tr("Save as")
                enabled: editor.modified
                onTriggered: {
                    var props = {
                        fileSelectorMode: false,
                        folderSelectorMode: true,
                        path: folderView.path
                    };
                    saveAsMode = true;
                    // fileSelector.fileSelectorComponent = pageStack.push(Qt.resolvedUrl("./FolderListPage.qml"), props)
                    var fileSelector = pageStack.push(Qt.resolvedUrl("./FolderListPage.qml"), props);
                }
            },
            Action {
                iconName: "filters"
                text: i18n.tr("Settings")

                onTriggered: PopupUtils.open(Qt.resolvedUrl("EditorViewPopover.qml"), mainView)
            },
            Action {
                iconName: "find"
                text: i18n.tr("Search")
                // visible: textArea.canUndo
                onTriggered: searchBarVisible = !searchBarVisible
            },
            Action {
                iconName: "edit-undo"
                text: i18n.tr("Undo")
                visible: textArea.canUndo
                onTriggered: textArea.undo()
            },
            Action {
                iconName: "edit-redo"
                text: i18n.tr("Redo")
                visible: textArea.canRedo
                onTriggered: textArea.redo()
            }
        ]

        extension: Row {
            visible: searchBarVisible
            anchors.left: parent.left
            anchors.leftMargin: units.gu(1)
            anchors.right: parent.right
            anchors.rightMargin: units.gu(1)
            height: searchBarVisible ? searchField.height + units.gu(1) : 0

            spacing: units.gu(1)

            TextField {
                id: searchField
                // anchors.left: parent.left
                // anchors.right: parent.right
                width: parent.width - units.gu(1) - searchActions.width

                placeholderText: "Search..."

                property bool hasResults: false

                onTextChanged: {
                    if (text != "") {
                        // console.log("searching", text);
                        var pos = editor.findString(text, textArea.selectionStart);
                        // console.log(pos);
                        if (pos == -1) pos = editor.findString(text, 0);

                        if (pos != -1) {
                            textArea.select(pos - text.length, pos);
                            hasResults = true;
                        } else {
                            hasResults = false;
                            console.log("not fount");
                        }
                    } else {
                        hasResults = false;
                        textArea.deselect();
                    }
                }
            }

            ActionBar {
                id: searchActions
                height: searchField.height

                actions: [
                    Action {
                        iconName: "go-down"
                        enabled: searchField.hasResults
                        onTriggered: {
                            if (searchField.text != "") {
                                // console.log("go down");
                                var pos = editor.findString(searchField.text, textArea.selectionEnd);
                                if (pos == -1) pos = editor.findString(searchField.text, 0);

                                if (pos != -1)
                                    textArea.select(pos - searchField.text.length, pos);
                                else
                                    console.log("not fount");
                            }
                        }
                    },
                    Action {
                        iconName: "go-up"
                        enabled: searchField.hasResults
                        onTriggered: {
                            if (searchField.text != "") {
                                // console.log("go up");
                                var pos = editor.findString(searchField.text, textArea.selectionStart, true);
                                if (pos == -1) pos = editor.findString(searchField.text, textArea.text.length, true);

                                if (pos != -1)
                                    textArea.select(pos - searchField.text.length, pos);
                                else
                                    console.log("not fount");
                            }
                        }
                    }
                ]
            }
        }
    }

    Editor {
        id: editor

        filePath: model.filePath
        document: textArea.textDocument

        // onLineHeightsChanged: console.log(editor.lineHeights);
    }

    TextArea {
        id: textArea

        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: Qt.inputMethod.keyboardRectangle.height

        onCursorVisibleChanged: {
            // console.log("Cursor visible", cursorVisible)
            cursorVisible = true;
        }
        cursorVisible: true

        wrapMode: editorSettings.wrapText ? TextEdit.Wrap : TextEdit.NoWrap
    }
}
