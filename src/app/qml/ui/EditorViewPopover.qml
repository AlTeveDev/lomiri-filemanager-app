import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import "../components" as Components

Dialog {

    __closeOnDismissAreaPress: true

    Component.onCompleted: {
        __foreground.itemSpacing = units.gu(0)
    }

    ListItem {
        ListItemLayout {
            anchors { left: parent.left; right: parent.right }
            anchors.leftMargin: units.gu(-2)
            anchors.rightMargin: units.gu(-2)

            subtitle.text: i18n.tr("Wrap big lines")
            subtitle.textSize: Label.Medium

            Switch{
                SlotsLayout.position: SlotsLayout.Last

                checked: editorSettings.wrapText
                onCheckedChanged: editorSettings.wrapText = checked
            }
        }
    }

    Components.HorizontalOptionSelector {
        subtitle: i18n.tr("Theme")
        selectedIndex: globalSettings.theme
        model: [ i18n.tr("System"), i18n.tr("Light"), i18n.tr("Dark") ]
        onSelectedIndexChanged: globalSettings.theme = selectedIndex
    }
}
