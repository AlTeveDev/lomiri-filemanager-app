/*
 * Copyright (C) 2014 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author : Niklas Wenzel <nikwen.developer@gmail.com>
 */

#include "editor.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QTextBlock>
#include <QAbstractTextDocumentLayout>

// Editor::Editor() {
//     m_quickDocument = new QQuickTextDocument(this);
// }

void Editor::setFilePath(QString filePath) {
    if (m_filePath.isEmpty()) {
        m_filePath = filePath;

        if (m_quickDocument)
            openFile();

        emit filePathChanged();
    } else if (m_filePath != filePath) {
        m_filePath = filePath;

        emit filePathChanged();
    }
}

QString Editor::filePath() {
    return m_filePath;
}

QString Editor::fileName() {
    QStringList list = m_filePath.split("/");
    return list[list.size()-1];
}

void Editor::setDocument(QQuickTextDocument *document) {
    qDebug() << "set document";
    if (m_quickDocument != document) {
        m_quickDocument = document;

        // m_modified = m_quickDocument->textDocument()->isModified();
        // connect(m_quickDocument->textDocument(), &QTextDocument::modificationChanged, this, [=](bool changed) {
        //     qDebug() << "set modified";
        //     m_modified = changed;
        //     emit modifiedChanged();
        // });
        connect(m_quickDocument->textDocument(), &QTextDocument::modificationChanged, this, &Editor::modifiedChanged);
        // connect(m_quickDocument->textDocument(), &QTextDocument::documentLayoutChanged, this, &Editor::lineHeightsChanged);
        // connect(m_quickDocument->textDocument(), &QTextDocument::blockCountChanged, this, &Editor::lineCountChanged);
        // connect(m_quickDocument->textDocument()->documentLayout(), &QAbstractTextDocumentLayout::documentSizeChanged, this, &Editor::lineHeightsChanged);
        if (!m_filePath.isEmpty())
            openFile();
    }
}

QQuickTextDocument *Editor::document() {
    return m_quickDocument;
}

bool Editor::modified() {
    if (m_quickDocument)
        return m_quickDocument->textDocument()->isModified();

    return false;
}

// int Editor::lineCount() {
//     if (!m_quickDocument)
//         return 0;
//     return m_quickDocument->textDocument()->blockCount();
// }
//
// QList<int> Editor::lineHeights() {
//     QList<int> heights = QList<int>();
//
//     if (m_quickDocument) {
//         // qDebug() << m_quickDocument->textDocument()->blockCount();
//         for (int i = 0; i < m_quickDocument->textDocument()->blockCount(); i++) {
//             QTextBlock block = m_quickDocument->textDocument()->findBlockByNumber(i);
//             heights.append(m_quickDocument->textDocument()->documentLayout()->blockBoundingRect(block).height());
//         }
//     }
//
//     return heights;
// }

void Editor::saveFile() {
    if (m_quickDocument && !m_filePath.isEmpty()) {
        writeFile(m_filePath, m_quickDocument->textDocument()->toPlainText());
        m_quickDocument->textDocument()->setModified(false);
    }
}

// void Editor::saveFileAs(QString path) {
//     if (m_filePath != path) {
//         m_filePath = path;
//     }
//     saveFile();
// }

QString Editor::readFile(const QString path) {
    // qDebug() << "readFile";
    QFile file(path);
    file.open(QIODevice::ReadOnly);
    QTextStream textStream(&file);
    QString string;
    while (!textStream.atEnd()) {
        string += textStream.readLine() + "\n";
    }
    return string;
}

void Editor::writeFile(const QString path, const QString fileContent) {
    // qDebug() << "readFile";
    QFile file(path);
    file.open(QIODevice::WriteOnly);
    QTextStream textStream(&file);
    textStream << fileContent;
}

void Editor::openFile() {
    qDebug() << "Open full";
    m_quickDocument->textDocument()->setPlainText(readFile(m_filePath));
    m_quickDocument->textDocument()->setModified(false);

    // emit lineCountChanged();
    // emit lineHeightsChanged();
}

int Editor::findString(QString query, int searchStart, bool reversed) {
    QTextDocument::FindFlags flags;
    if (reversed) flags = flags | QTextDocument::FindBackward;
    QTextCursor cursor = m_quickDocument->textDocument()->find(query, searchStart, flags);
    // qDebug() << searchStart;
    // qDebug() << cursor.position() - query.length();
    return cursor.position();
}
