/*
 * Copyright (C) 2014 Canonical Ltd
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author : Niklas Wenzel <nikwen.developer@gmail.com>
 */

#ifndef EDITOR_H
#define EDITOR_H

#include <QObject>
// #include <QProcess>
#include <QDebug>
#include <QTextDocument>
#include <QQuickTextDocument>

class Editor : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString filePath READ filePath WRITE setFilePath NOTIFY filePathChanged)
    Q_PROPERTY(QString fileName READ fileName NOTIFY filePathChanged)
    Q_PROPERTY(QQuickTextDocument *document READ document WRITE setDocument)
    Q_PROPERTY(bool modified READ modified NOTIFY modifiedChanged)
    // Q_PROPERTY(int lineCount READ lineCount NOTIFY lineCountChanged)
    // Q_PROPERTY(QList<int> lineHeights READ lineHeights NOTIFY lineHeightsChanged)

public:
    // explicit Editor();

    void setFilePath(QString filePath);
    QString filePath();

    QString fileName();

    void setDocument(QQuickTextDocument *document);
    QQuickTextDocument *document();

    bool modified();

    // int lineCount();
    // QList<int> lineHeights();

    Q_INVOKABLE void saveFile();
    // Q_INVOKABLE void saveFileAs(const QString path);
    /*Q_INVOKABLE*/ QString readFile(const QString path);
    /*Q_INVOKABLE*/ void writeFile(const QString path, const QString fileContent);
    void openFile();

    Q_INVOKABLE int findString(QString query, int searchStart, bool reversed = false);

signals:
    void filePathChanged();
    void modifiedChanged();
    // void lineCountChanged();
    // void lineHeightsChanged();

private:
    QString m_filePath;

    QQuickTextDocument *m_quickDocument = nullptr;
};


#endif // EDITOR_H
