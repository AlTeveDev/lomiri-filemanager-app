#include "linenumbershelper.h"

#include <QAbstractTextDocumentLayout>
#include <QDebug>
#include <QTextBlock>

void LineNumbersHelper::setDocument(QQuickTextDocument *document) {
    qDebug() << "set document";
    if (m_quickDocument != document) {
        m_quickDocument = document;

        // m_modified = m_quickDocument->textDocument()->isModified();
        // connect(m_quickDocument->textDocument(), &QTextDocument::modificationChanged, this, [=](bool changed) {
        //     qDebug() << "set modified";
        //     m_modified = changed;
        //     emit modifiedChanged();
        // });
        // connect(m_quickDocument->textDocument(), &QTextDocument::modificationChanged, this, &Editor::modifiedChanged);
        // connect(m_quickDocument->textDocument(), &QTextDocument::documentLayoutChanged, this, &Editor::lineHeightsChanged);
        connect(m_quickDocument->textDocument(), &QTextDocument::blockCountChanged, this, &LineNumbersHelper::lineCountChanged);
        connect(m_quickDocument->textDocument()->documentLayout(), &QAbstractTextDocumentLayout::documentSizeChanged, this, &LineNumbersHelper::lineHeightsChanged);

        emit lineCountChanged();
        emit lineHeightsChanged();
    }
}

QQuickTextDocument *LineNumbersHelper::document() {
    return m_quickDocument;
}

int LineNumbersHelper::lineCount() {
    if (!m_quickDocument)
        return 0;
    return m_quickDocument->textDocument()->blockCount();
}

QList<int> LineNumbersHelper::lineHeights() {
    QList<int> heights = QList<int>();

    if (m_quickDocument) {
        // qDebug() << m_quickDocument->textDocument()->blockCount();
        for (int i = 0; i < m_quickDocument->textDocument()->blockCount(); i++) {
            QTextBlock block = m_quickDocument->textDocument()->findBlockByNumber(i);
            heights.append(m_quickDocument->textDocument()->documentLayout()->blockBoundingRect(block).height());
        }
    }

    return heights;
}

int LineNumbersHelper::blockNumberFromPos(int pos) {
    return m_quickDocument->textDocument()->findBlock(pos).blockNumber();
}

int LineNumbersHelper::selectionStart() {
    return m_selectionStart;
}
void LineNumbersHelper::setSelectionStart(int selectionStart) {
    if (m_selectionStart != selectionStart) {
        if (blockNumberFromPos(m_selectionStart) != blockNumberFromPos(selectionStart))
            m_firstSelectedBlock = blockNumberFromPos(selectionStart);
            emit firstSelectedBlockChanged();

        m_selectionStart = selectionStart;
        emit selectionStartChanged();
    }
}

int LineNumbersHelper::selectionEnd() {
    return m_selectionEnd;
}
void LineNumbersHelper::setSelectionEnd(int selectionEnd) {
    if (m_selectionEnd != selectionEnd) {
        if (blockNumberFromPos(m_selectionEnd) != blockNumberFromPos(selectionEnd))
            m_lastSelectedBlock = blockNumberFromPos(selectionEnd);
            emit lastSelectedBlockChanged();

        m_selectionEnd = selectionEnd;
        emit selectionEndChanged();
    }
}

int LineNumbersHelper::firstSelectedBlock() {
    return m_firstSelectedBlock;
}
int LineNumbersHelper::lastSelectedBlock() {
    return m_lastSelectedBlock;
}
// LineNumbersHelper::LineNumbersHelper(QObject *parent) : QObject(parent)
// {
//
// }
//
// QObject* LineNumbersHelper::document()
// {
//     return this->m_document;
// }
//
// void LineNumbersHelper::setDocument(QObject *p)
// {
//     QQuickTextDocument* pointer = qobject_cast<QQuickTextDocument*>(p);
//
//     if (!pointer) {
//         qWarning() << "Provided pointer is not of type QQuickTextDocument";
//         return;
//     }
//
//     if (this->m_document == pointer)
//         return;
//
//     if (this->m_document) {
//         QObject::disconnect(this->m_document->textDocument(), &QTextDocument::blockCountChanged,
//                             this, &LineNumbersHelper::lineCountChanged);
//     }
//
//     this->m_document = pointer;
//     QObject::connect(this->m_document->textDocument(), &QTextDocument::blockCountChanged,
//                      this, &LineNumbersHelper::lineCountChanged);
//     emit documentChanged();
// }
//
// int LineNumbersHelper::lineCount()
// {
//     // qDebug() << "getting line count";
//         return 0;
//
//     return this->m_document->textDocument()->blockCount();
// }
//
// int LineNumbersHelper::height(int lineNumber)
// {
//     if (!this->m_document)
//         return 0;
//
//     QTextBlock textBlock = this->m_document->textDocument()->findBlockByNumber(lineNumber);
//     int ret = int(this->m_document->textDocument()->documentLayout()->blockBoundingRect(textBlock).height());
//     return ret;
// }
//
// bool LineNumbersHelper::isCurrentBlock(int blockNumber, int curserPosition)
// {
// //    qDebug() << "isCurrentBlock" << blockNumber;
//     if (!this->m_document)
//         return false;
//
//     QTextBlock block = this->m_document->textDocument()->findBlock(curserPosition);
//     QTextBlock line = this->m_document->textDocument()->findBlockByNumber(blockNumber);
//     return block == line;
// }
