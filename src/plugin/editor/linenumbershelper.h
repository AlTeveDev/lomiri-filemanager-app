#ifndef LINENUMBERSHELPER_H
#define LINENUMBERSHELPER_H

#include <QObject>
#include <QQuickTextDocument>

class LineNumbersHelper : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQuickTextDocument *document READ document WRITE setDocument)
    Q_PROPERTY(int lineCount READ lineCount NOTIFY lineCountChanged)
    Q_PROPERTY(QList<int> lineHeights READ lineHeights NOTIFY lineHeightsChanged)
    Q_PROPERTY(int selectionStart READ selectionStart WRITE setSelectionStart NOTIFY selectionStartChanged)
    Q_PROPERTY(int selectionEnd READ selectionEnd WRITE setSelectionEnd NOTIFY selectionEndChanged)
    Q_PROPERTY(int firstSelectedBlock READ firstSelectedBlock NOTIFY firstSelectedBlockChanged)
    Q_PROPERTY(int lastSelectedBlock READ lastSelectedBlock NOTIFY lastSelectedBlockChanged)

public:
    void setDocument(QQuickTextDocument *document);
    QQuickTextDocument *document();

    int lineCount();
    QList<int> lineHeights();

    int selectionStart();
    void setSelectionStart(int selectionStart);
    int selectionEnd();
    void setSelectionEnd(int selectionEnd);
    int firstSelectedBlock();
    int lastSelectedBlock();

    // Q_PROPERTY(QObject* document READ document WRITE setDocument NOTIFY documentChanged)
    // Q_PROPERTY(int lineCount READ lineCount NOTIFY lineCountChanged)
    //
    // explicit LineNumbersHelper(QObject *parent = nullptr);
    //
    // Q_INVOKABLE int lineCount();
    // Q_INVOKABLE int height(int lineNumber);
    // Q_INVOKABLE bool isCurrentBlock(int blockNumber, int curserPosition);
    //
    // QObject* document();
    // void setDocument(QObject* p);

signals:
    void lineCountChanged();
    void lineHeightsChanged();

    void selectionStartChanged();
    void selectionEndChanged();
    void firstSelectedBlockChanged();
    void lastSelectedBlockChanged();

private:
    int blockNumberFromPos(int pos);

    QQuickTextDocument *m_quickDocument = nullptr;

    int m_selectionStart;
    int m_selectionEnd;
    int m_firstSelectedBlock;
    int m_lastSelectedBlock;

// signals:
//     void documentChanged();
//     void lineCountChanged();

};

#endif // LINENUMBERSHELPER_H
