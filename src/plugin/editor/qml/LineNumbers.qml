import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.FileManager.Editor 0.1

Item {
    id: root

    property font font
    // property alias document: lineNumbersHelper.document
    property int cursorPosition

    // property var palsette

    property int lineCount
    property var lineHeights

    property color lineColor
    property color activeLineColor

    property int firstSelectedBlock
    property int lastSelectedBlock

    // TODO: optimize
    // function refreshLines() {
    //     repeater.model = 0;
    //     repeater.model = lineNumbersHelper.lineCount;
    // }

    width: column.width + units.gu(1)
    height: column.height

    // LineNumbersHelper {
    //     id: lineNumbersHelper
    //
        property int lineCountChars
        onLineCountChanged: {
            var nums = lineCount;
            var chars = 0;
            while (nums >= 1) {
                nums = nums/10;
                chars += 1;
            }
            lineCountChars = chars;
        }
    // }

    TextMetrics {
        id: textMetrics

        font.family: font.family
        font.pixelSize: font.pixelSize

        text: "4"
    }

    Column {
        id: column

//        anchors.horizontalCenter: root.horizontalCenter
        anchors.right: parent.right
        width: textMetrics.width * /*lineNumbersHelper*/root.lineCountChars

        Repeater {
            id: repeater
            // Component.onCompleted: model = /*lineNumbersHelper*/root.lineCount
            model: root.lineCount
            delegate: Text {
                readonly property bool isCurrentLine:
                    index >= firstSelectedBlock && index <= lastSelectedBlock
                    // lineNumbersHelper.isCurrentBlock(index, root.cursorPosition)

                anchors.right: column.right
                // color: "white"
                color: isCurrentLine ?
                    root.activeLineColor :
                    root.lineColor
                //             root.palette.label :
                //             root.palette.lineNumber
                // height: lineNumbersHelper.height(index)
                height: lineHeights[index]
                // font.family: settings.font
                // font.pixelSize: settings.fontSize
                text: index + 1
            }
        }
    }
}
